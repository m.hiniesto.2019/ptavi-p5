# Practica 5: Sesion SIP


Ejercicio 3. Análisis general
===========
---
¿Cuántos paquetes componen la captura?\
¿Cuánto tiempo dura la captura?\
¿Qué IP tiene la máquina donde se ha efectuado la captura?\
¿Se trata de una IP pública o de una IP privada?\
¿Por qué lo sabes?

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. 
En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el número de paquetes y el 
tráfico correspondiente a los distintos protocolos.

¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?\
¿Qué otros protocolos podemos ver en la jerarquía de protocolos?\
¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?

Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos 
viendo corresponde con una llamada SIP.
Filtra por sip para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.

¿En qué segundos tienen lugar los dos primeros envíos SIP?\
Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?\
Los paquetes RTP, ¿cada cuánto se envían?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.


## Respuesta
### ¿Cuántos paquetes componen la captura?

la captura esta compuesta por 1050 paquetes.

### ¿Cuánto tiempo dura la captura?
Segun las estadisticas de la captura, ha transcurrido un periodo de 10.527 segundos.

### ¿Qué IP tiene la máquina donde se ha efectuado la captura?
La ip de la maquina donde se efectua la captura es 192.168.1.116

### ¿Se trata de una IP pública o de una IP privada?
Se trata de una ip privada.

### ¿Por qué lo sabes?
Las ip privadas suelen comenzar por 10, 172 y 192, en este caso empieza por 192.

### ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
Los tres protocolos son UDP, SIP y RTP.

### ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
Podemos ver el protocolo de nivel de enlase Ethernet, El protocolo de nivel de red IPv4 y el protocolo de nivel de red ICMP.

### ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo?¿Cuánto es ese tráfico?
El que mas trafico presenta es RTP con una media de 134 kbits/s.

### ¿En qué segundos tienen lugar los dos primeros envíos SIP?
En el segundo 0.

### Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
El paquete numero 6 es el ptimero de la trama.

### Los paquetes RTP, ¿cada cuánto se envían?
Cada 10 milisegundos.



Enunciado 4. Primeras tramas
===========
---
Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a 
la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y 
"Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren 
a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.

¿De qué protocolo de nivel de aplicación son?\
¿Cuál es la dirección IP de la máquina "Linphone"?\
¿Cuál es la dirección IP de la máquina "Servidor"?\
¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?\
¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?

Ahora, veamos las dos tramas siguientes.

¿De qué protocolo de nivel de aplicación son?\
¿Entre qué máquinas se envía cada trama?\
¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?\
¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md 
(respondiendo en una nueva sección de respuesta, como se ha indicado).

Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta
### ¿De qué protocolo de nivel de aplicación son?
Son de l protocolo de nivel de aplicacion SIP.

### ¿Cuál es la dirección IP de la máquina "Linphone"?
192.168.1.116

### ¿Cuál es la dirección IP de la máquina "Servidor"?
212.79.111.155

### ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
La primera trama se envia porque Linphone quiere establecer una 
comunicacion VoIP con music@sip.iptel.org, envia una request al servidor.

### ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
El servidor envia un mensaje con el codigo 100 Traying al Linphone.

### ¿De qué protocolo de nivel de aplicación son?
Siguen siendo del protocola SIP.

### ¿Entre qué máquinas se envía cada trama?
el primero se envia de el servidor a Linphone y el segundo de Linphone a el servidor.

### ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
La primera de ellas es el codigo 200 ok, lo que expresa que el servidor ha recivido correctamente su peticion.

### ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
La segunda de ellas es un ACK de Linphone a el servidor.



Ejercicio 5. Tramas finales
===========
---
Después de la trama 250, busca la primera trama SIP.

¿Qué número de trama es?\
¿De qué máquina a qué máquina va?\
¿Para qué sirve?\
¿Puedes localizar en ella qué versión de Linphone se está usando?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.
## Respuesta
### ¿Qué número de trama es?
La primera trama SIP tras el paquete 250 es la numero 1042.

### ¿De qué máquina a qué máquina va?
De la maquina 192.168.1.116 (Linphone) a la maquina 212.79.111.155 (servidor).

### ¿Para qué sirve?
Sirve para cerrar la comunicacion.

### ¿Puedes localizar en ella qué versión de Linphone se está usando?
Linphone Desktop/4.3.2 (Debian GNU/Linux bookworm/sid, Qt 5.15.6) LinphoneCore/5.0.37



Ejercicio 6, Invitación
===========
---
Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

¿Cuál es la direccion SIP con la que se quiere establecer una llamada?\
¿Qué instrucciones SIP entiende el UA?\
¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?\
¿Cuál es el nombre de la sesión SIP?

## Respuesta
### ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
Se quiere establecer la llamada con music@sip.iptel.org

### ¿Qué instrucciones SIP entiende el UA?
Segun nuestra cabezera Allow las instrucciones que entiende son: 
INVITE, ACK, CANCEL, OPTIONS, BYE, REFER,  NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE.

### ¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?
La cabezera content-type

### ¿Cuál es el nombre de la sesión SIP?
Talk.


Ejercicio 7. Indicación de comienzo de conversacion
===========
---
En la propuesta SDP de Linphone puede verse un campo m con un valor que empieza por audio 7078.

¿Qué trama lleva esta propuesta?\
¿Qué indica el 7078?\
¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?\
¿Qué paquetes son esos?

En la respuesta a esta propuesta vemos un campo m con un valor que empieza por audio XXX.

¿Qué trama lleva esta respuesta?\
¿Qué valor es el XXX?\
¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?\
¿Qué paquetes son esos?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.



## Respuesta
### ¿Qué trama lleva esta propuesta?
la trama que lleva es la numero 2, sin filtrado SIP.

### ¿Qué indica el 7078?
Indica la direccion de transporte.

### ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
Los paquetes que veremos mas adelante envian o reciven las tramas por ese puerto(7078), dependiendo de si es el servidor o el cliente.

### ¿Qué paquetes son esos?
los paquetes RTP

### ¿Qué trama lleva esta respuesta?
La trama numero 4, sin filtrado SIP.

### ¿Qué valor es el XXX?
29448

### ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
Los paquetes que veremos mas adelante envian o reciven las tramas por ese puerto(29448), dependiendo de si es el servidor o el cliente.

### ¿Qué paquetes son esos?
Los paquetes RTP


Ejercicio 8. Primeros paquetes RTP
===========
---
Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

¿De qué máquina a qué máquina va?\
¿Qué tipo de datos transporta?\
¿Qué tamaño tiene?\
¿Cuántos bits van en la "carga de pago" (payload)\
¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta
### ¿De qué máquina a qué máquina va?
De linphone(192.168.1.116) a el servidor(212.79.111.155).

### ¿Qué tipo de datos transporta?
Datos de flujo multimedia.

### ¿Qué tamaño tiene?
214 bytes.

### ¿Cuántos bits van en la "carga de pago" (payload)?
Solo 160 bytes, 1280 bits.

### ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
Se envia un paquete RTP cada 10 ms


Ejercicio 9. Fujos RTP
===========
---
Vamos a ver más a fondo el intercambio RTP. Busca en el menú Telephony la opción RTP. Empecemos mirando los flujos RTP.

¿Cuántos flujos hay? ¿por qué?\
¿Cuántos paquetes se pierden?\
Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?\
¿Qué es lo que significa el valor de delta?\
¿En qué flujo son mayores los valores de jitter (medio y máximo)?\
¿Qué significan esos valores?\

Vamos a ver ahora los valores de una trama concreto, la númro 27. Vamos a analizarla en opción Telephony, RTP, RTP Stream Analysis:

¿Cuánto valen el delta y el jitter para ese paquete?\
¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?\
El "skew" es negativo, ¿qué quiere decir eso?

En el panel Stream Analysis puedes hacer play sobre los streams:

¿Qué se oye al pulsar play?\
¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?\
¿A qué se debe la diferencia?\

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta
### ¿Cuántos flujos hay? ¿por qué?
Hay dos flujos RTP, uno cliente -> servidor y el otro servidor -> cliente.

### ¿Cuántos paquetes se pierden?
Hay 0 paquetes perditos en esta transmision

### Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
30,7267 ms

### ¿Qué es lo que significa el valor de delta?
Delta es la diferencia del tiempo de envio entre paquete y paquete.

### ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
En el flujo del servidor a Linphone

### ¿Qué significan esos valores?
Estos valores nos indican la variacion que puede haber entre distintos retardos

### ¿Cuánto valen el delta y el jitter para ese paquete?
Delta vale 0.000164 y el jitter 3.087182

### ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
Si, mirando el valor Skew

### El "skew" es negativo, ¿qué quiere decir eso?
Quiere decir que nuestro paquete ha llegado con 4.527123 ms de antelacion. 

### ¿Qué se oye al pulsar play?
Escuchamos una melodia de un par de instrumentos

### ¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?
Se escuchan muchos cortes en el audio.

### ¿A qué se debe la diferencia?
Con un jitter buffer mas pequeño, se espera durante menos tiempo a los paquetes de la sequencia que faltan por llegar, si ha pasado 
este tiempo y no han llegado, se descartan ,de ahi el corte. 1 ms es un valor muy reducido, teniendo encuenta que los valores
medios del jitter son 1.55 ms en el primer flujo y 2.99 ms en el segundo flujo.


Ejercicio 10. Llamada VoIP
===========
---
Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú Telephony selecciona el menú VoIP calls, y selecciona la llamada hasta que te salga el panel correspondiente:

¿Cuánto dura la llamada?

Ahora, pulsa sobre Flow Sequence:

Guarda el diagrama en un fichero (formato PNG) con el nombre diagrama.png.\
¿En qué segundo se recibe el último OK que marca el final de la llamada?

Ahora, selecciona los dos streams, y pulsa sobre Play Sterams, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

¿Cuáles son las SSRC que intervienen?\
¿Cuántos paquetes se envían desde LinPhone hasta Servidor?\
¿Cuál es la frecuencia de muestreo del audio?\
¿Qué formato se usa para los paquetes de audio (payload)?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.
## Respuesta 

### ¿Cuánto dura la llamada?
Dura 10 segundos.
### ¿En qué segundo se recibe el último OK que marca el final de la llamada?
Se recibe en el segundo 10.516

### ¿Cuáles son las SSRC que intervienen?
Los valores SSRC son 0x0d2db8b4 para el primer flujo(Linphone -> Servidor) y 0x5c44a34b para el segundo flujo(Servidor -> Linphone).

### ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
Se envian 514 paquetes

### ¿Cuál es la frecuencia de muestreo del audio?
El valor SR (Sample rate) es de 8 KHz

### ¿Qué formato se usa para los paquetes de audio (payload)?
Se usa el formato de audio g711U

Ejerciio 11
===========
---
Desde LinPhone, créate una cuenta SIP.  A continuación:

* Captura una llamada VoIP con el identificador SIP sip:music@sip.iptel.org, de unos 10 
segundos de duración. Comienza a capturar
tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el 
fichero linphone-music.pcapng. Asegura (usando los filtros antes de guardarla) que en la 
captura haya solo paquetes entre la máquina donde has hecho la captura y has ejecutado 
LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.\
* ¿Cuántos flujos RTP tiene esta captura?\
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Respuesta

### ¿Cuántos flujos RTP tiene esta captura?

La captura tiene dos flujos RTP, uno de mi maquina a el servidor y otro del servidor a mi maquina.
### ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?

El valor maximo de delta en el flujo 1 es de 36.33\
El valor maximo de delta en el flujo 2 es de 32.39\
El valor medio del jitter en el flujo 1 de de 0.04\
El valor medio de jitter en el flujo 2 es de 0.03\
El valor maximo del jitter en el fujo 1 es de 3.59\
El valor maximo del jitter en el flujo 2 es de 6.42
